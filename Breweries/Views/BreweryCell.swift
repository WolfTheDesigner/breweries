//
//  BreweryCell.swift
//  Breweries
//
//  Created by Wolf on 22.02.2019.
//  Copyright © 2019 Волк. All rights reserved.
//

import UIKit

class BreweryCell: UITableViewCell {
    
    // MARK: - Properties
    public weak var delegate: BrowserDelegate?
    public var id = Int()

    
    // MARK: - IBOutlets
    @IBOutlet weak var breweryNameLabel: UILabel!
    
    
    // MARK: - Actions
    @IBAction func siteButton(_ sender: UIButton) {
        self.delegate?.openInBrowser(id: self.id)
    }
}
