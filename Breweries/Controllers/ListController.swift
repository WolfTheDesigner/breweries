//
//  ListController.swift
//  Breweries
//
//  Created by Wolf on 22.02.2019.
//  Copyright © 2019 Волк. All rights reserved.
//

import UIKit
import SafariServices

class ListController: UIViewController, UITableViewDataSource, UITableViewDelegate, BrowserDelegate, DataDelegate {
    
    // MARK: - Properties
    private var data = DataModel()
    
    
    // MARK: - IBOutlets
    @IBOutlet weak var breweriesTableView: UITableView!
    
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Network.delegate = self
        self.data.dataDelegate = self
        self.breweriesTableView.dataSource = self
        self.breweriesTableView.delegate = self
    }
    
    
    // MARK: Delegate methods
    public func openInBrowser(id: Int) {
        if let url = URL(string: self.data[id].url) {
            let vc = SFSafariViewController(url: url)
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    // This method is called from Data Model when next part of data is loaded
    func update() {
        self.breweriesTableView.reloadData()
    }
    
    // MARK: - Data Source 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // I use force type casting here to debug errors during development. It can not crash here if the code is OK:
        let cell = tableView.dequeueReusableCell(withIdentifier: "breweryItemCell", for: indexPath) as! BreweryCell
        
        // Cell configuration:
        cell.id = indexPath.row
        cell.delegate = self
        cell.breweryNameLabel.text = self.data[indexPath.row].name
        
        return cell
    }
    
    // Pagination:
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // If the last loaded item is going to be displayed - load more:
        if indexPath.row == (self.data.skip - 1) {
            self.data.load()
        }
    }
}

