//
//  LoginController.swift
//  Breweries
//
//  Created by Wolf on 23.02.2019.
//  Copyright © 2019 Волк. All rights reserved.
//

import UIKit

class LoginController: UIViewController {
    
    // MARK: -  Properties
    private let keychain = KeychainStorage()

    // MARK: - IBOutlets
    @IBOutlet weak var loginOutlet: UITextField!
    @IBOutlet weak var passwordOutlet: UITextField!
    @IBOutlet weak var buttonOutlet: UIButton!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.authWithKeychain()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Methods
    private func authWithKeychain() {
        if self.keychain.exist {
            if let login = self.keychain.login, let password = self.keychain.password {
                self.authorize(login: login, password: password)
            }
            else { self.keychain.clear() }
        }
    }

    private func authorize(login: String, password: String, handler: (() -> ())? = nil) {
        Network.authorize(login: login, password: password) { [weak self] (status) in
            switch status {
            case .unauthorized: break
            case .unknown: self?.performSegue(withIdentifier: "ShowBreweries", sender: nil)
            case .authorized:
                self?.performSegue(withIdentifier: "ShowBreweries", sender: nil)
                handler?()
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func loginAction(_ sender: UIButton) {
        Network.delegate = self
        guard let login = self.loginOutlet.text else { return }
        guard let password = self.passwordOutlet.text else { return }
        self.authorize(login: login, password: password) { [weak self] in
            self?.keychain.save(login: login, password: password)
        }
    }
}
