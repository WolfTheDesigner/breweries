//
//  ErrorDelegate+Extensions.swift
//  Breweries
//
//  Created by Wolf on 23.02.2019.
//  Copyright © 2019 Волк. All rights reserved.
//

import UIKit

protocol ErrorDelegate: class {
    func alert(error: String)
}

extension UIViewController: ErrorDelegate {
    public func alert(error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}
