//
//  DataDelegate.swift
//  Breweries
//
//  Created by Wolf on 22.02.2019.
//  Copyright © 2019 Волк. All rights reserved.
//

import Foundation

protocol DataDelegate: class {
    func update()
}
