//
//  DataModel.swift
//  Breweries
//
//  Created by Wolf on 22.02.2019.
//  Copyright © 2019 Волк. All rights reserved.
//

import Foundation

class DataModel {
    
    public weak var dataDelegate: DataDelegate?
    public weak var errorDelegate: ErrorDelegate? {
        didSet {
            self.coreDataSource.errorDelegate = self.errorDelegate
        }
    }
    // Data is stored here:
    private var coreDataSource = CoreDataSource()
    private var breweriesArray = Brewery()
    
    // Property "skip" is used for pagination - amount of items to skip in API request:
    public var skip: Int = 0
    
    public subscript(index: Int) -> BreweryElement {
        get {
            return breweriesArray[index]
        }
    }
    
    public var count: Int {
        get {
            return breweriesArray.count
        }
    }
    
    // Load data:
    public func load() {
        switch Network.status{
        // If network is online an user is authorized: load from network
        case .authorized:
            Network.getBreweries(skip: self.skip) { [weak self] (result) in
                if let result = result {
                    self?.breweriesArray += result
                    // Save loaded data to CoreData database:
                    self?.coreDataSource.add(breweries: result)
                    self?.skip += 20
                    self?.dataDelegate?.update()
                }
            }
        // If network is offline: load from CoreData
        case .unknown:
            coreDataSource.getBreweries(skip: self.skip) { [weak self] (result) in
                if let result = result {
                    self?.breweriesArray += result
                    self?.skip += 20
                    self?.dataDelegate?.update()
                }
            }
        case .unauthorized: break
        }
    }
    
    init() {
        self.load()
    }
}
