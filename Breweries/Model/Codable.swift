//
//  DataModel.swift
//  Breweries
//
//  Created by Wolf on 22.02.2019.
//  Copyright © 2019 Волк. All rights reserved.
//

import Foundation

// Codable structure for JSON parsing

typealias Brewery = [BreweryElement]

struct BreweryElement: Codable {
    let id: Int
    let name: String
    let url: String
    
//    init(id: Int, name: String, url: String) {
//        self.id = id
//        self.name = name
//        self.url = url
//    }
}

struct User: Codable {
    let userId: Int
}
