//
//  Network.swift
//  Breweries
//
//  Created by Wolf on 22.02.2019.
//  Copyright © 2019 Волк. All rights reserved.
//

import Foundation
import Alamofire

enum NetworkStatus {
    case unknown
    case authorized
    case unauthorized
}

class Network {
    typealias responseHandler = (Brewery?) -> ()
    typealias loginHandler = (NetworkStatus) -> ()
    
    // MARK: - Properties
    public static var status: NetworkStatus = .unknown
    
    public static weak var delegate: ErrorDelegate?

    private static let dataUrl = "https://private-0d719-brewery1.apiary-mock.com/breweries?"
    private static let authUrl = "https://private-0d719-brewery1.apiary-mock.com/login?"
    //private static let dataUrl = "https://www.reintodev.cz:4006/api/breweries?"
    //private static let authUrl = "https://www.reintodev.cz:4006/api/login"
    private static var userId = Int()
    
    // MARK: - Methods
    private static func handleDataResponse(data: Data?) -> Brewery? {
        if let data = data {
            do {
                return try JSONDecoder().decode(Brewery.self, from: data)
            } catch let error{
                Network.delegate?.alert(error: error.localizedDescription)
            }
        }
        return nil
    }
    
    private static func handleAuthResponse(data: Data?) {
        if let data = data {
            do {
                Network.userId = try JSONDecoder().decode(User.self, from: data).userId
                Network.status = .authorized
            } catch let error {
                Network.delegate?.alert(error: error.localizedDescription)
            }
        }
    }
    
    // Authentication request:
    public static func authorize(login: String, password: String, handler: @escaping loginHandler) {
        // Request parameters:
        let parameters: Parameters = [
            "login": login,
            "password": password
        ]
        Alamofire.request(Network.authUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseData { (responseData) in
            if Network.checkStatus(responseData: responseData) {
                Network.handleAuthResponse(data: responseData.data)
            }
            handler(Network.status)
        }
    }
    
    // Data request:
    public static func getBreweries(skip: Int, handler: @escaping responseHandler) {
        // Request parameters:
        let url = self.dataUrl + "userId=\(Network.userId)&skip=\(skip)"        
        Alamofire.request(url).responseData { (responseData) in
            if Network.checkStatus(responseData: responseData) {
                handler(Network.handleDataResponse(data: responseData.data))
            }
        }
    }
    
    // Check response status:
    private static func checkStatus(responseData: DataResponse<Data>) -> Bool {
        var success = false
        switch responseData.result {
        case .failure(let error):
            Network.delegate?.alert(error: error.localizedDescription)
        case .success(_):
            guard let status = responseData.response?.statusCode else {
                Network.delegate?.alert(error: "Unknown error.")
                break
            }
            switch status {
            case 200: success = true
            case 401:
                Network.delegate?.alert(error: "Wrong username or password.")
                Network.status = .unauthorized
            default: Network.delegate?.alert(error: "Error! Response status: \(status).")
            }
        }
        return success
    }
}
