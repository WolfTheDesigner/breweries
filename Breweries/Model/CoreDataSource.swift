//
//  CoreDataSource.swift
//  Breweries
//
//  Created by Wolf on 24.02.2019.
//  Copyright © 2019 Волк. All rights reserved.
//

import UIKit
import CoreData

class CoreDataSource {
    typealias responseHandler = (Brewery?) -> ()
    
    // MARK: - Properties
    public weak var errorDelegate: ErrorDelegate?
    private let context: NSManagedObjectContext
    // Data loaded from database is stored here:
    private var data: [BreweryEntity]?
    
    // MARK: - Methods
    // Method to get data from CoreData. Interface is the same as in Network class:
    public func getBreweries(skip: Int, handler: @escaping responseHandler) {
        if let data = self.data {
            if skip < data.count {
                var dataToReturn = Brewery()
                var counter = skip
                while counter < (skip + 20) && (counter < data.count) {
                    dataToReturn.append(BreweryElement(id: Int(data[counter].id), name: data[counter].name!, url: data[counter].url!))
                    counter += 1
                }
                handler(dataToReturn)
            }
        }
        handler(nil)
    }
    
    // Adds network loaded data to CoreData:
    public func add(breweries: Brewery) {
        for brewery in breweries {
            let entity = BreweryEntity(context: self.context)
            entity.id = Int64(brewery.id)
            entity.name = brewery.name
            entity.url = brewery.url
        }
        do {
            try self.context.save()
        } catch let error {
            self.errorDelegate?.alert(error: error.localizedDescription)
        }
    }
    
    // Loads data from database to class storage:
    private func loadData() {
        let fetchRequest: NSFetchRequest<BreweryEntity> = BreweryEntity.fetchRequest()
        do {
            try self.data = context.fetch(fetchRequest)
        }
        catch let error {
            self.errorDelegate?.alert(error: error.localizedDescription)
        }
    }
    
    // Deletes all data from database:
    private func deleteData() {
        if let data = self.data {
            for entity in data {
                context.delete(entity)
            }
        }
        self.data = nil
    }
    
    init() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { fatalError("Fatal error while Core Data initialization!") }
        self.context = appDelegate.persistentContainer.viewContext
        self.loadData()
        // Delete all data from DB if user is online. The data will be replaced by new one loaded from network.
        if Network.status == .authorized {
            self.deleteData()
        }
    }
}
