//
//  KeychainStorage.swift
//  Breweries
//
//  Created by Wolf on 24.02.2019.
//  Copyright © 2019 Волк. All rights reserved.
//

import Foundation
import KeychainSwift

// Class KeychainStorage is used for secure storing user's login and password in local device's keychain after successful login. User doesn't need to enter login and password next time.
class KeychainStorage {
    
    // MARK: - Properties
    private let keychain = KeychainSwift()
    
    public var exist: Bool {
        get {
            guard let status = self.keychain.getBool("LoginStatus") else { return false }
            return status
        }
    }
    
    public var login: String? {
        get {
            return self.keychain.get("Login")
        }
    }
    
    public var password: String? {
        get {
            return self.keychain.get("Password")
        }
    }
    
    // MARK: - Methods
    public func save(login: String, password: String) {
        self.keychain.set(true, forKey: "LoginStatus")
        self.keychain.set(login, forKey: "Login")
        self.keychain.set(password, forKey: "Password")
    }
    
    public func clear() {
        self.keychain.clear()
    }
}
